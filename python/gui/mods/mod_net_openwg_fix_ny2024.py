from debug_utils import LOG_CURRENT_EXCEPTION
from gui.Scaleform.daapi.settings.views import VIEW_ALIAS
from helpers import dependency
from skeletons.gui.impl import IGuiLoader
from gui.Scaleform.framework.entities.sf_window import SFWindow

try:
	from new_year import ny_tutorial_controller

	def isInHangar():
		guiLoader = dependency.instance(IGuiLoader)
		openWindows = guiLoader.windowsManager.findWindows(ny_tutorial_controller.findWindowPredicate)
		if not openWindows:
			return False
		hasHangar = False
		for window in openWindows:
			# have any popup or offer opened
			if not isinstance(window, SFWindow):
				return False
			hasHangar |= window.loadParams.viewKey.alias == VIEW_ALIAS.LOBBY_HANGAR
		return hasHangar

	ny_tutorial_controller.isInHangar = isInHangar

except ImportError:
	pass
except:
	LOG_CURRENT_EXCEPTION()
